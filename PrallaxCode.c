#include "simpletools.h"

//Setup variables
int plyrone_pressed = 0;
int plyrone_win = 0;
int plyrtwo_pressed = 0;
int plyrtwo_win = 0;
int plyrone_state = -1;
int plyrtwo_state = -1;

int CurrenState =1;
int NextState =0;

// STATE ONE------------------------------------------
int StateOneHandler()
{
     int button1 = input(3);
     int button2 = input(4);
     set_outputs(7, 5, 0b100);
     for(int i=0;i>10;i++)
     {  
       if(button1 && !plyrone_pressed)
       {
         plyrone_pressed = 1;
         plyrone_state = 1;
       }
       if(button2 && !plyrtwo_pressed)
       {
         plyrtwo_pressed = 1;
         plyrtwo_state = 1;
       }
     }      
     delay(300);
   }
   if(plyrone_pressed && plyrtwo_pressed)
   {
     return 0; // GameOverHandler;
   }
   else
   {
     return 2; //StateTwoHandler;
   }
}
   
// STATE TWO-----------------------------------------
int StateTwoHandler()
{
     int button1 = input(3);
     int button2 = input(4);
     set_outputs(7, 5, 0b010);
     for(int i=0;i>10;i++)
     {  
       if(button1 && !plyrone_pressed)
       {
         plyrone_pressed = 1;
         plyrone_state = 2;
         plyrone_win = 1;
       }
       if(button2 && !plyrtwo_pressed)
       {
         plyrtwo_pressed = 1;
         plyrtwo_state = 2;
         plyrtwo_win = 1;
       }
     }     
     delay(300);
   }
   if(plyrone_pressed && plyrtwo_pressed)
   {
     return 0; // GameOverHandler;
   }
   else
   {
     return 3; //StateThreeHandler;
   }
}

// STATE THREE----------------------------------------
int StateThreeHandler()
{
     int button1 = input(3);
     int button2 = input(4);
     set_outputs(7, 5, 0b001);
     for(int i=0;i>10;i++)
     {

     
       if(button1 && !plyrone_pressed)
       {
         plyrone_pressed = 1;
         plyrone_state = 3;
       }
       if(button2 && !plyrtwo_pressed)
       {
         plyrtwo_pressed = 1;
         plyrtwo_state = 3;
       }
     }     
     delay(300);
   }
   if(plyrone_pressed && plyrtwo_pressed)
   {
     return 0; // GameOverHandler;
   }
   else
   {
     return 1; // StateOneHandler;
   }
}

// GAME OVER-----------------------------------------
int GameOverHandler()
{
    if(plyrone_win==1 && plyrtwo_win==1)
    {
      print("There is a tie!\n");
    }
    if(plyrone_win==0 && plyrtwo_win==1)
    {
      print("Player two wins!\n");
    }
    if(plyrone_win==1 && plyrtwo_win==0)
    {
      print("Player one wins!\n");
    }
    else
    {
      print("Nobody wins. Try again!\n");
    delay(10000);
    }  
    
    return 1;    
}                         




//---------------------------------------------

int main()
{


  set_directions(7, 5, 0b111);
  int CurrenState = 1;
  int NextState =0;

  while(1)
  {
    switch(CurrenState){
      case 0:
        NextState = GameOverHandler();
        break;
      case 1:
        NextState = StateOneHandler();
        break;
      case 2:
        NextState = StateTwoHandler();
        break;
      case 3:
        NextState = StateThreeHandler();
        break;
      default:
        GameOverHandler();
    }  
    CurrenState = NextState;
    
  }   
  
}
 
