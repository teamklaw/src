Player1 = 0
Player2 = 0

while Player1 <= 4 and Player2 <= 4:
    if Player1 == 4:
        print('Player 1 wins. \nPlayer 2 was put in a win-win situation and still lost.')
        break
    elif Player2 == 4:
        print('Player 2 wins. \nPlayer 1, the attempt was fine.... ')
        break
    else:
        x = input("Who Scored? ")
        if x == '1':
            Player1 += 1
            print("Player 1: %d \nPlayer 2: %d"%(Player1, Player2))
        elif x == '2':
            Player2 += 1
            print("Player 1: %d \nPlayer 2: %d"%(Player1, Player2))
        elif x =='tie':
            Player1 += 1
            Player2 += 1
            print("Congrats, y'all both won, still an L.... \nPlayer 1: %d \nPlayer 2: %d"%(Player1, Player2))
        elif x =='0':
            Player1 += 0
            Player2 += 0
            print("Nobody won, Loserz! \nPlayer 1: %d \nPlayer 2: %d"%(Player1, Player2))
        else:
            print('Please input: 0, 1, 2, or tie')